import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { VariablesEnum } from './common/enums/variables.enum';
import { ConfigSwagger } from './configuration/main.config';
import { AppModule } from './entities/app/app.module';
import * as Sentry from '@sentry/node';
import { NodeEnvironmentEnum } from './common/enums/node-environment.enum';
import { HttpExceptionFilter } from './common/filters/http-exeption.filter';
import { SanitizeEntityInterceptor } from './common/interceptors/sanitize.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['error', 'warn'],
  });

  // Добавляем конфигурацию main файла
  const configService = app.get(ConfigService);

  const PORT = configService.get<number>(VariablesEnum.PORT, 80);
  const API_VERSION = configService.get<string>(VariablesEnum.API_VERSION);
  const DOMAIN = configService.get<string>(VariablesEnum.DOMAIN, 'localhost');
  const SWAGGER_PATH = configService.get<string>(VariablesEnum.SWAGGER_PATH);
  const SENTRY_DSN = configService.get<string>(VariablesEnum.SENTRY_DSN);
  const NODE_ENV = configService.get<string>(VariablesEnum.NODE_ENV);

  // Настраиваем Swagger
  const swaggerDocument = SwaggerModule.createDocument(app, ConfigSwagger);
  SwaggerModule.setup(`${API_VERSION}/${SWAGGER_PATH}`, app, swaggerDocument);

  // Настраиваем Sentry
  Sentry.init({
    dsn: SENTRY_DSN,
    environment: NODE_ENV,
    debug: NODE_ENV === NodeEnvironmentEnum.PROD ? true : false,
  });

  // Настраиваем глобальные сущности
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new SanitizeEntityInterceptor());

  // Настраиваем запуск сервера
  app.setGlobalPrefix(`${API_VERSION}`);
  await app.listen(PORT);

  // Вывод простых логов
  console.log(`Server started at port: ${PORT}`);
  console.log(`Server listen ${DOMAIN}:${PORT}/${API_VERSION}`);
}
bootstrap();
