import { DocumentBuilder } from '@nestjs/swagger';

export const ConfigSwagger = new DocumentBuilder()
  .setTitle('Anilist API')
  .setDescription('API документация к проекту Anilist')
  .setVersion('1.0')
  .addBearerAuth()
  .build();
