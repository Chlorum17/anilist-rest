import { Module } from '@nestjs/common';
import { ListModel } from './configuration/list.config';
import { ListQuery } from './database/list.query';

@Module({
  imports: [ListModel],
  providers: [ListQuery],
})
export class ListModule {}
