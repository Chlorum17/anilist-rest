import { MongooseModule } from '@nestjs/mongoose';
import { List, ListSchema } from '../database/list.repository';

export const ListModel = MongooseModule.forFeature([
  { schema: ListSchema, name: List.name },
]);
