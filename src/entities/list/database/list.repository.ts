import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Title } from 'src/entities/title/database/title.repository';
import { User } from 'src/entities/user/database/user.repository';

export type ListDocument = mongoose.Document & List;

@Schema()
export class List {
  @Prop({
    unique: true,
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  })
  user: User;

  @Prop({
    unique: true,
    required: true,
    default: [],
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Title',
  })
  plans: Title[];

  @Prop({
    unique: true,
    required: true,
    default: [],
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Title',
  })
  watching: Title[];

  @Prop({
    unique: true,
    required: true,
    default: [],
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Title',
  })
  abandoned: Title[];

  @Prop({
    unique: true,
    required: true,
    default: [],
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Title',
  })
  viewed: Title[];
}

export const ListSchema = SchemaFactory.createForClass(List);
