import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { List, ListDocument } from './list.repository';

@Injectable()
export class ListQuery extends CoreQuery<ListDocument> {
  constructor(
    @InjectModel(List.name) private readonly listModel: Model<ListDocument>,
  ) {
    super(listModel);
  }
}
