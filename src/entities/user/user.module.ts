import { Module } from '@nestjs/common';
import { UserModel } from './configuration/user.config';
import { UserQuery } from './database/user.query';

@Module({
  imports: [UserModel],
  providers: [UserQuery],
})
export class UserModule {}
