import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { User, UserDocument } from './user.repository';

@Injectable()
export class UserQuery extends CoreQuery<UserDocument> {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {
    super(userModel);
  }
}
