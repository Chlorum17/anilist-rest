import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from '../database/user.repository';

export const UserModel = MongooseModule.forFeature([
  { schema: UserSchema, name: User.name },
]);
