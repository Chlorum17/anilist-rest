import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type RoleDocument = Document & Role;

@Schema()
export class Role {
  @Prop({ unique: true, required: true })
  title: string;

  @Prop({ unique: true, required: true })
  name: string;
}

export const RoleSchema = SchemaFactory.createForClass(Role);
