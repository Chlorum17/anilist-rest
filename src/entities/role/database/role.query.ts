import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Role, RoleDocument } from './role.repository';

@Injectable()
export class RoleQuery extends CoreQuery<RoleDocument> {
  constructor(
    @InjectModel(Role.name) private readonly roleModel: Model<RoleDocument>,
  ) {
    super(roleModel);
  }
}
