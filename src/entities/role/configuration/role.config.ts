import { MongooseModule } from '@nestjs/mongoose';
import { Role, RoleSchema } from '../database/role.repository';

export const RoleModel = MongooseModule.forFeature([
  { schema: RoleSchema, name: Role.name },
]);
