import { Module } from '@nestjs/common';
import { RoleModel } from './configuration/role.config';
import { RoleQuery } from './database/role.query';

@Module({
  imports: [RoleModel],
  providers: [RoleQuery],
})
export class RoleModule {}
