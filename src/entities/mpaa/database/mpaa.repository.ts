import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type MpaaDocument = Document & Mpaa;

@Schema()
export class Mpaa {
  @Prop({ unique: true, required: true })
  title: string;

  @Prop({ required: true })
  description: string;
}

export const MpaaSchema = SchemaFactory.createForClass(Mpaa);
