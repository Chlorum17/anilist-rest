import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Mpaa, MpaaDocument } from './mpaa.repository';

@Injectable()
export class MpaaQuery extends CoreQuery<MpaaDocument> {
  constructor(
    @InjectModel(Mpaa.name) private readonly mpaaModel: Model<MpaaDocument>,
  ) {
    super(mpaaModel);
  }
}
