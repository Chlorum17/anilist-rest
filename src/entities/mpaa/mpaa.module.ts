import { Module } from '@nestjs/common';
import { MpaaModel } from './configuration/mpaa.config';
import { MpaaQuery } from './database/mpaa.query';

@Module({
  imports: [MpaaModel],
  providers: [MpaaQuery],
})
export class MpaaModule {}
