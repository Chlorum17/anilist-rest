import { MongooseModule } from '@nestjs/mongoose';
import { Mpaa, MpaaSchema } from '../database/mpaa.repository';

export const MpaaModel = MongooseModule.forFeature([
  { schema: MpaaSchema, name: Mpaa.name },
]);
