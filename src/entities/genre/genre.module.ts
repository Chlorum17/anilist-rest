import { Module } from '@nestjs/common';
import { GenreModel } from './configuration/genre.config';
import { GenreQuery } from './database/genre.query';

@Module({
  imports: [GenreModel],
  providers: [GenreQuery],
})
export class GenreModule {}
