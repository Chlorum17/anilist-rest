import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Genre, GenreDocument } from './genre.repository';

@Injectable()
export class GenreQuery extends CoreQuery<GenreDocument> {
  constructor(
    @InjectModel(Genre.name) private readonly genreModel: Model<GenreDocument>,
  ) {
    super(genreModel);
  }
}
