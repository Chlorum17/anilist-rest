import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type GenreDocument = Document & Genre;

@Schema()
export class Genre {
  @Prop({ unique: true, required: true })
  title: string;

  @Prop({ required: true })
  description: string;
}

export const GenreSchema = SchemaFactory.createForClass(Genre);
