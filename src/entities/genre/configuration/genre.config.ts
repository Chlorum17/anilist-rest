import { MongooseModule } from '@nestjs/mongoose';
import { Genre, GenreSchema } from '../database/genre.repository';

export const GenreModel = MongooseModule.forFeature([
  { schema: GenreSchema, name: Genre.name },
]);
