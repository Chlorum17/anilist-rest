import { Module } from '@nestjs/common';
import { GenreModule } from '../genre/genre.module';
import { ImageModule } from '../image/image.module';
import { MpaaModule } from '../mpaa/mpaa.module';
import { StudioModule } from '../studio/studio.module';
import { RoleModule } from '../role/role.module';
import { TypeModule } from '../type/type.module';
import {
  ConfigModuleRoot,
  MongooseModuleRoot,
} from './configuration/app.config';
import { UserModule } from '../user/user.module';
import { Title } from '../title/database/title.repository';
import { ListModule } from '../list/list.module';

@Module({
  imports: [
    ConfigModuleRoot,
    MongooseModuleRoot,
    GenreModule,
    MpaaModule,
    TypeModule,
    ImageModule,
    StudioModule,
    RoleModule,
    UserModule,
    Title,
    ListModule,
  ],
})
export class AppModule {}
