import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { VariablesEnum } from 'src/common/enums/variables.enum';
import { VariablesInterface } from 'src/common/interfaces/variables.interface';

export const ConfigModuleRoot = ConfigModule.forRoot({ isGlobal: true });

export const MongooseModuleRoot = MongooseModule.forRootAsync({
  imports: [ConfigModuleRoot],
  useFactory: async (configService: ConfigService<VariablesInterface>) => {
    const URI = configService.get<string>(VariablesEnum.DBURI);

    return {
      uri: URI,
      useCreateIndex: true,
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    };
  },
  inject: [ConfigService],
});
