import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Title, TitleDocument } from './title.repository';

@Injectable()
export class TitleQuery extends CoreQuery<TitleDocument> {
  constructor(
    @InjectModel(Title.name) private readonly titleModel: Model<TitleDocument>,
  ) {
    super(titleModel);
  }
}
