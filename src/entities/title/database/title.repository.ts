import { Prop, Schema, SchemaFactory, raw } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Genre } from 'src/entities/genre/database/genre.repository';
import { Mpaa } from 'src/entities/mpaa/database/mpaa.repository';
import { Studio } from 'src/entities/studio/database/studio.repository';
import { Type } from 'src/entities/type/database/type.repository';

export type TitleDocument = mongoose.Document & Title;

@Schema()
export class Title {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  description: string;

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'Genre' })
  genres: Genre[];

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'Mpaa' })
  mpaa: Mpaa;

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'Studio' })
  studio: Studio;

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'Type' })
  type: Type;

  @Prop({ required: true, default: 0.0 })
  rating: number;

  @Prop({ required: true, enum: ['Анонсированно', 'Выходит', 'Вышло'] })
  status: string;

  @Prop({ required: true })
  date: Date;

  @Prop({ required: true })
  licensor: string;

  @Prop({ required: true })
  image: string;

  @Prop({ required: true })
  alternativeNames: string[];

  @Prop(
    raw({
      japanese: { type: String },
      latin: { type: String },
    }),
  )
  originalNames: Record<string, string>;
}

export const TitleSchema = SchemaFactory.createForClass(Title);
