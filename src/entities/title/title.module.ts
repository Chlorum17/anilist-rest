import { Module } from '@nestjs/common';
import { TitleModel } from './configuration/title.config';
import { TitleQuery } from './database/title.query';

@Module({
  imports: [TitleModel],
  providers: [TitleQuery],
})
export class TitleModule {}
