import { MongooseModule } from '@nestjs/mongoose';
import { Title, TitleSchema } from '../database/title.repository';

export const TitleModel = MongooseModule.forFeature([
  { schema: TitleSchema, name: Title.name },
]);
