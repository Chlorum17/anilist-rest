import { Module } from '@nestjs/common';
import { TypeModel } from './configuration/type.config';
import { TypeQuery } from './database/type.query';

@Module({
  imports: [TypeModel],
  providers: [TypeQuery],
})
export class TypeModule {}
