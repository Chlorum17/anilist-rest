import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type TypeDocument = Document & Type;

@Schema()
export class Type {
  @Prop({ unique: true, required: true })
  title: string;

  @Prop({ required: true })
  description: string;
}

export const TypeSchema = SchemaFactory.createForClass(Type);
