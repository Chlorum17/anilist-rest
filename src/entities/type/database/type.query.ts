import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Type, TypeDocument } from './type.repository';

@Injectable()
export class TypeQuery extends CoreQuery<TypeDocument> {
  constructor(
    @InjectModel(Type.name) private readonly typeModel: Model<TypeDocument>,
  ) {
    super(typeModel);
  }
}
