import { MongooseModule } from '@nestjs/mongoose';
import { Type, TypeSchema } from '../database/type.repository';

export const TypeModel = MongooseModule.forFeature([
  { schema: TypeSchema, name: Type.name },
]);
