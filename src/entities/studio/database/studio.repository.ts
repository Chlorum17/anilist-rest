import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Image } from 'src/entities/image/database/image.repository';

export type StudioDocument = mongoose.Document & Studio;

@Schema()
export class Studio {
  @Prop({ unique: true, required: true })
  title: string;

  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'Image' })
  image: Image;
}

export const StudioSchema = SchemaFactory.createForClass(Studio);
