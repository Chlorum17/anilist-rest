import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Studio, StudioDocument } from './studio.repository';

@Injectable()
export class StudioQuery extends CoreQuery<StudioDocument> {
  constructor(
    @InjectModel(Studio.name)
    private readonly studioModel: Model<StudioDocument>,
  ) {
    super(studioModel);
  }
}
