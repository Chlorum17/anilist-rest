import { Module } from '@nestjs/common';
import { StudioModel } from './configuration/studio.config';
import { StudioQuery } from './database/studio.query';

@Module({
  imports: [StudioModel],
  providers: [StudioQuery],
})
export class StudioModule {}
