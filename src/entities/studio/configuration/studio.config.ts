import { MongooseModule } from '@nestjs/mongoose';
import { Studio, StudioSchema } from '../database/studio.repository';

export const StudioModel = MongooseModule.forFeature([
  { schema: StudioSchema, name: Studio.name },
]);
