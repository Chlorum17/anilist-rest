import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ImageDocument = Document & Image;

@Schema()
export class Image {
  @Prop({ required: true })
  extension: string;

  @Prop({ unique: true, required: true })
  hash: string;
}

export const ImageSchema = SchemaFactory.createForClass(Image);
