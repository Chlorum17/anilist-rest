import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CoreQuery } from 'src/common/core/core.query';
import { Image, ImageDocument } from './image.repository';

@Injectable()
export class ImageQuery extends CoreQuery<ImageDocument> {
  constructor(
    @InjectModel(Image.name) private readonly imageModel: Model<ImageDocument>,
  ) {
    super(imageModel);
  }
}
