import { Module } from '@nestjs/common';
import { ImageModel } from './configuration/image.config';
import { ImageQuery } from './database/image.query';

@Module({
  imports: [ImageModel],
  providers: [ImageQuery],
})
export class ImageModule {}
