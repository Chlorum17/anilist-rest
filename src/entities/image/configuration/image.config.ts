import { MongooseModule } from '@nestjs/mongoose';
import { Image, ImageSchema } from '../database/image.repository';

export const ImageModel = MongooseModule.forFeature([
  { schema: ImageSchema, name: Image.name },
]);
