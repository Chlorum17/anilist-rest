import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class SanitizeEntityInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      map((data) => {
        let preparedData;
        const meta = data.meta || null;

        if (data instanceof Array) {
          preparedData = data.map((entity) => this.sanitizeEntity(entity));
        }

        preparedData = this.sanitizeEntity(data);

        return {
          data: preparedData,
          error: null,
          meta,
        };
      }),
    );
  }

  private sanitizeEntity(data) {
    if (data._id) {
      const { _id, __v, ...rest } = data.toObject();
      return {
        id: _id,
        ...rest,
      };
    }
    return data;
  }
}
