import { CoreQuery } from './core.query';
import { PopulateOptions, FilterQuery, UpdateQuery, Document } from 'mongoose';

export abstract class CoreService<T extends Document, U extends CoreQuery<T>> {
  protected constructor(protected readonly query: U) {}

  async createOne(doc: Record<string, any>): Promise<T> {
    const document = await this.query.createOne(doc);
    return document;
  }

  async createMany(docs: Record<string, any>[]): Promise<T[]> {
    const newDocuments = await this.query.createMany(docs);
    return newDocuments;
  }

  async findMany(
    filter: FilterQuery<T>,
    populate?: PopulateOptions,
    sort?: Record<string, any>,
    limit?: number,
    skip?: number,
  ): Promise<T[] | []> {
    const documents = await this.query.findMany(
      filter,
      populate,
      sort,
      limit,
      skip,
    );
    return documents;
  }

  async findOne(
    filter: FilterQuery<T>,
    populate?: PopulateOptions,
  ): Promise<T | null> {
    const document = await this.query.findOne(filter, populate);
    return document;
  }

  async findById(id: string, populate?: PopulateOptions): Promise<T | null> {
    const document = await this.query.findById(id, populate);
    return document;
  }

  async findOneAndUpdate(
    filter: FilterQuery<T>,
    update: UpdateQuery<T>,
    populate?: PopulateOptions,
  ): Promise<T | null> {
    const updatedDocument = await this.query.findOneAndUpdate(
      filter,
      update,
      populate,
    );
    return updatedDocument;
  }

  async findByIdAndUpdate(
    id: string,
    update: UpdateQuery<T>,
    populate?: PopulateOptions,
  ): Promise<T | null> {
    const updatedDocument = await this.query.findByIdAndUpdate(
      id,
      update,
      populate,
    );
    return updatedDocument;
  }

  async deleteMany(filter: FilterQuery<T>): Promise<any> {
    const result = await this.query.deleteMany(filter);
    return result;
  }

  async deleteOne(filter: FilterQuery<T>): Promise<T> {
    const deletedDocument = await this.query.deleteOne(filter);
    return deletedDocument;
  }

  async deleteById(id: string): Promise<T> {
    const deletedDocument = await this.query.deleteById(id);
    return deletedDocument;
  }
}
