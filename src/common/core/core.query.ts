import {
  Model,
  Document,
  PopulateOptions,
  FilterQuery,
  UpdateQuery,
} from 'mongoose';

export abstract class CoreQuery<T extends Document> {
  protected constructor(protected readonly model: Model<T>) {}

  async createOne(doc: Record<string, any>): Promise<T> {
    const document = await this.model.create(doc);
    return document;
  }

  async createMany(docs: Record<string, any>[]): Promise<T[]> {
    const newDocuments = await this.model.create(docs);
    return newDocuments;
  }

  async findMany(
    filter: FilterQuery<T>,
    populate?: PopulateOptions,
    sort?: Record<string, any>,
    limit?: number,
    skip?: number,
  ): Promise<T[] | []> {
    const documents = await this.model
      .find(filter)
      .populate(populate)
      .sort(sort)
      .limit(limit)
      .skip(skip);
    return documents;
  }

  async findOne(
    filter: FilterQuery<T>,
    populate?: PopulateOptions,
  ): Promise<T | null> {
    const document = await this.model.findOne(filter).populate(populate);
    return document;
  }

  async findById(id: string, populate?: PopulateOptions): Promise<T | null> {
    const document = await this.model.findById(id).populate(populate);
    return document;
  }

  async findOneAndUpdate(
    filter: FilterQuery<T>,
    update: UpdateQuery<T>,
    populate?: PopulateOptions,
  ): Promise<T | null> {
    const updatedDocument = await this.model
      .findOneAndUpdate(filter, update, { new: true })
      .populate(populate);
    return updatedDocument;
  }

  async findByIdAndUpdate(
    id: string,
    update: UpdateQuery<T>,
    populate?: PopulateOptions,
  ): Promise<T | null> {
    const updatedDocument = await this.model
      .findByIdAndUpdate(id, update, { new: true })
      .populate(populate);
    return updatedDocument;
  }

  async deleteMany(filter: FilterQuery<T>): Promise<any> {
    const result = await this.model.deleteMany(filter);
    return result;
  }

  async deleteOne(filter: FilterQuery<T>): Promise<T> {
    const deletedDocument = await this.model.findOneAndDelete(filter);
    return deletedDocument;
  }

  async deleteById(id: string): Promise<T> {
    const deletedDocument = await this.model.findByIdAndDelete(id);
    return deletedDocument;
  }
}
