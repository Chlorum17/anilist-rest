export interface VariablesInterface {
  AL_PORT: number;
  AL_API_VERSION: string;
  AL_SERVER_DOMAIN: string;
  AL_DATABASE_URI: string;
  AL_SWAGGER_PATH: string;
  AL_SENTRY_DSN: string;
  AL_NODE_ENV: string;
}
